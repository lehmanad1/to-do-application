'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Task Schema
 */
var TaskSchema = new Schema({
	name:{
		type: String,
		required:'Please enter a task name',
		trim:true
	},
	completed:{
		type: Boolean,
		default: false
	},
	created: {
  type: Date,
  default: Date.now
 },
 user: {
  type: Schema.ObjectId,
  ref: 'User'
 }
});

mongoose.model('Task', TaskSchema);