'use strict';
angular.module('core').controller('HomeController', ['$scope', 'Authentication',
	function($scope, Authentication) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		
		
		function Task(taskDescription){
				this.description = taskDescription;
				this.completed = false;
				this.TaskCompleted = function(){
					this.completed = true;	
				};
				this.TaskNotCompleted = function (){
					this.completed = false;
				};
				this.ChangeDescription = function(newDescription){
					this.description = newDescription;
				};
		}

		$scope.RemoveItem = function(index, TaskList){
			TaskList.splice(index,1);
		};

		
		$scope.addTask = function(taskDescription, taskList){
			taskList[taskList.length] = new Task(taskDescription);
		};
		
		$scope.Tasks = [
			new Task('Create false data to test AngularJS Frontend.'),
			new Task('Create MongoDB database to create REAL backend items.')
		];
	}
]);